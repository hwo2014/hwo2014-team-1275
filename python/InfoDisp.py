import matplotlib.pyplot as plt
import sys


def graphData(filename):
	inFile=open(filename, "r") 
	data = inFile.readlines()

	''' Data in columns
	col1 = []
	col2 = []
	col3 = []
	col4 = []
	col5 = []
	header = []
	with open(filename) as f:
		header = f.readline().split()
		for line in f.readlines():
			#line = line.strip()
			#print line
			row = line.strip().split()
			col1.append(float(row[0]))
			col2.append(float(row[1]))
			col3.append(float(row[2]))
			col4.append(float(row[3]))
			col5.append(float(row[4]))
	'''

	#data in rows
	colors = ['r', 'g', 'b', 'm', 'c', 'y', 'b', 'b', 'b']
	with open(filename) as f:
		header = f.readline().split()
		x = map(float, f.readline().strip().split())
		for i in range(0, len(header) - 1):
			y = map(float, f.readline().strip().split())
			plt.plot(x, y, colors[i])

	plt.legend(header[1:])
	plt.show()

#--------------------------
if __name__ == '__main__':
	if len(sys.argv) != 2:
		print("Usage: ./InfoDisp data/datafile.txt")
	else:
		datafile = sys.argv[1]
		print "Parsing file", datafile
		graphData(datafile)
        