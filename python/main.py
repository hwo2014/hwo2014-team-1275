import json
import socket
import sys
import math

import datetime

import CleanCarState as CleanCarStateClass #hack, needs to be cleaned up.
import CarState as CarStateClass

from Model import *

from hwoutil import getTurnRadius
from InfoBot import InfoBot

VERBOSE = False

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.color = "COLOR"

        self.mytrack = {}
        self.lapsRemaining = 99
        #Ideal line for Keimola
        self.myActionList = [ "Right", "None", "Left", "Right", "None", "None", "None", "None", "None", "Left", "Right", "None", "None", "None", "None", "None", "Left", "Right", "None", "None", "None", "None", "None" ]
        self.lastSwitchUsed = -1
        self.lastLocID = -1
        self.trackPieceCount = 0
        self.laneInfo = []
        self.myThrottle = 0.45
        self.turboReady = False
        self.turboData = {}
        self.turboData["TicksLeft"] = 0
        self.turboData["Active"] = False
        self.turboData["Ready"] = False
        self.turboData["Power"] = 1

        self.rivalCars = {} #Hold data for all the other cars
        self.factorRivals = False; #Temp Global >.< It's bad I know 

        self.gameTick = 0.0
        self.carStates = False
        self.isCrashed = False


        #---model stuff---
        self.myCarId = None
        self.model = None
        self.cleanCarStates = None
        
        self.DataInitialized = False
        
    #-----Piece Utility Methods---
    # Calculate the length of a given piece given the lane id
    #
    # If the piece is a straight, just return length
    # If the piece is a turn, calculate the length taking laneId into account
    def getPieceLength(self, pieceIndex, startLaneIndex, endLaneIndex):
        if ('length' in self.myTrack[pieceIndex]):
            return float(self.myTrack[pieceIndex]['length'])
        else:
            radius = self.getTurnRadius(pieceIndex, startLaneIndex, endLaneIndex)
            return abs((self.myTrack[pieceIndex]['angle']/360.0) * (2 * math.pi * (radius)))

    # Calculate the radius of a round piece, taking the lane offset into account
    # TODO - right now only using startLaneIndex
    def getTurnRadius(self, pieceIndex, startLaneIndex, endLaneIndex):
        for l in self.laneInfo:
            if l['index'] == startLaneIndex:
                laneOffset = float(l['distanceFromCenter']) * (self.myTrack[pieceIndex]['angle']/abs(self.myTrack[pieceIndex]['angle']))

        return self.myTrack[pieceIndex]['radius'] - laneOffset
    
    def distanceBetweenTrackPieces(self, trackA, trackB, lane):
        distance = 0
        while (trackA != trackB):
            distance += self.getPieceLength( trackA, lane, lane)
            trackA += 1
            if (trackA == self.trackPieceCount):
                trackA -= self.trackPieceCount
        return distance
    
    #-----Send Message Methods----
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        if(VERBOSE): print("in join()")
        return self.msg("join", {"name": self.name, "key": self.key})
        #return self.msg("joinRace", {"trackName": "keimola", "carCount":1,"botId":{"name": self.name,"key": self.key}})
        #return self.msg("joinRace", {"trackName": "france", "carCount":1,"botId":{"name": self.name,"key": self.key}})
        #return self.msg("joinRace", {"trackName": "germany", "carCount":1,"botId":{"name": self.name,"key": self.key}})
        #return self.msg("joinRace", {"trackName": "usa", "carCount":1,"botId":{"name": self.name,"key": self.key}})

    #------Handle Message Methods----
    def throttle(self, throttle):
        #print "throttle: ", throttle
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        if(VERBOSE): print("in run()")
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_your_car(self, data):
        print("Your car")
        self.myCarId = data

    def on_game_start(self, data):
        print("Race started")
        self.gameTick = 0
        self.ping()


            

    def on_car_positions(self, data, gameTick):
        #--calculate current state--

        #first game tic, get all car states
        if (self.carStates == False):
            self.carStates = CarStateClass.CarStates(self.myTrack, self.laneInfo, self.model, data, gameTick)

        else:
            self.carStates.updateState(data, gameTick)


        ##########LEARNING STAGE, MUST BE AT A CONSTANT THROTTLE FOR 4 TICKS############
        #--update the model, if in learning phase use constant throttle--
        if not(self.model.trainedLinearParameters):

            learningThrottle = 1.0

            #--calculate current state--
            #first game tic, get all car states
            if (self.cleanCarStates == None):
                self.cleanCarStates = CleanCarStateClass.CarStates(self.myTrack, self.laneInfo, data, gameTick)

            else:
                self.cleanCarStates.updateState(data, gameTick, self.myCarId, learningThrottle)
                self.model.trainModel(self.cleanCarStates)

            self.throttle(learningThrottle)
            pass
        ##################

        myCar = []
        for car in data:
            if (car["id"]["name"] == self.name):
                myCar = car
                self.color = car["id"]["color"]
            else:
                rivalCar = self.rivalCars.get(car["id"]["color"], {})
                #calculate new data based on old data
                rivalCar["speed"] = self.distanceBetweenTrackPieces(rivalCar.get("pieceIndex", 0),car["piecePosition"]["pieceIndex"], car["piecePosition"]["lane"]["endLaneIndex"])  - rivalCar.get("Position", 0) + car["piecePosition"]["pieceIndex"]  
                #update rival's data with current data
                rivalCar["angle"] = car["angle"]
                rivalCar["pieceIndex"] = car["piecePosition"]["pieceIndex"]
                rivalCar["lane"] = car["piecePosition"]["lane"]["endLaneIndex"]
                rivalCar["position"] = car["piecePosition"]["inPieceDistance"]
                rivalCar["futurePieceIndex"] = rivalCar["pieceIndex"]
                rivalCar["futureLane"] = rivalCar["lane"]
                self.rivalCars[ car["id"]["color"] ] = rivalCar

        #using Heather's Car State for Throttle Control
        myCarState = self.carStates.cars[self.color]
        #print("MyCar: Speed: {0} / {1}  Accel: {2}".format(myCarState.speed, myCarState.maxSpeed, myCarState.acceleration))




                
        myLocID = myCar["piecePosition"]["pieceIndex"]
        nextID = myLocID + 1
        if (nextID >= self.trackPieceCount): #Perform a switch command before the switch happens
            nextID -= self.trackPieceCount
        if (myLocID != self.lastLocID):
            self.lastLocID = myLocID
            #print("Car just entered: {0}".format(self.myTrack[myLocID]))

        #determine and send switch
        #!!! Determine when we need to recalculate the path (Before switch?  After? Both?)
        if (self.myActionList) and (self.myTrack[ nextID ].get("switch", False) == True) and (myLocID != self.lastSwitchUsed):
            #(self, aStartID, aLane, aEndID
            #print("StartLane Index {0}".format(myCar["piecePosition"]["lane"]["startLaneIndex"]))
            #project my rivals into the future
            for color in self.rivalCars:
                rival = self.rivalCars[color]
                if (rival["pieceIndex"] < myLocID):
                    continue
                self.factorRivals = False
                path = self.shortestPath(rival["pieceIndex"], rival["lane"], rival["pieceIndex"] + 5)
                rival["futurePieceIndex"] = path["pieceIndex"]
                rival["futureLane"] = path["lane"]

            self.factorRivals = True
            self.shortestPath(myLocID, myCar["piecePosition"]["lane"]["startLaneIndex"], myLocID+25)
            self.lastSwitchUsed = myLocID
            action = self.myActionList.pop(0)
            print("Switching to: {0}".format(action))
            if (action != "None"):
                self.msg("switchLane", action)
                return

        #Turbo is Active, how does this effect throttle
        if (self.turboData["Active"] == True):
            myCarState.turboActive = True
            self.turboData["TicksLeft"] -= 1
            if (self.turboData["TicksLeft"] <= 0):
                self.turboData["Active"] = False
                myCarState.turboActive = False
            self.throttle(self.myThrottle) #For now make no changes
            return
                
        
        #Find a target to bump
        turboTarget = False
        for color in self.rivalCars:
            rival = self.rivalCars[color]
            if (rival["pieceIndex"] < myLocID):
                continue
            if (rival["pieceIndex"] < (myLocID + 2)) and (rival["lane"] ==  myCar["piecePosition"]["lane"]):
                turboTarget = True

        #bump at full speed
        if (self.turboData["Ready"] == True) and (turboTarget == True):
            self.msg("turbo", "It's Turbo-tastic!")
            self.turboData["Ready"] = False
            self.turboData["Active"] = True
            print("TURBO TO BUMP!!!!")
            return
        

        #straightTrack = 0 TURBO IS DELAYED?!?!  http://www.reddit.com/r/HWO/comments/23qti0/2nd_turbo_not_triggering/
        straightTrack = 10 #TURBO IS DELAYED?!?!  http://www.reddit.com/r/HWO/comments/23qti0/2nd_turbo_not_triggering/
        straightTrack += math.fabs(self.getTrackAt(myLocID+0).get("angle", 0))
        straightTrack += math.fabs(self.getTrackAt(myLocID+1).get("angle", 0))
        straightTrack += math.fabs(self.getTrackAt(myLocID+2).get("angle", 0))
        straightTrack += math.fabs(self.getTrackAt(myLocID+3).get("angle", 0))
        straightTrack += math.fabs(self.getTrackAt(myLocID+4).get("angle", 0))

        if (straightTrack == 0) and (self.turboData["Ready"] == True):
                self.msg("turbo", "It's Turbo-tastic!")
                self.turboData["Ready"] = False
                self.turboData["Active"] = True
                print ("TURBO TO BOOST! {0}".format(straightTrack))
                return
        
        self.myThrottle = myCarState.currThrottle
                

        if (self.isCrashed == False): 
            self.throttle(self.myThrottle)
        else:
            self.ping()

    def on_crash(self, data, gameTick):
        print("Someone crashed")
        CarState = self.carStates.cars[data["color"]]
        CarState.crashed()

        if (self.color == data["color"]):
            self.isCrashed = True;

        self.model.trainFromCrash(self.carStates.cars[data["color"]])

        self.ping()

    def on_spawn(self, data):
        if (self.color == data["color"]):
            print("I spwaned");
            self.isCrashed = False
        else:
            print("Someone Else spawned")
        CarState = self.carStates.cars[data["color"]]
        CarState.spawned()



    def on_game_end(self, data):
        print("Race ended")
        self.DataInitialized = True
        self.gameTick = 0.0
        self.isCrashed = False
        self.carStates.reset()
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    #Create Data structure to hold the track data recieved
    def on_gameInit(self, data):

        # laps not defined for CI tests
        if ('laps' in data["race"]["raceSession"]):
            self.lapsRemaining = data["race"]["raceSession"]["laps"]
        print ("Laps Remaining: {0}". format(self.lapsRemaining))
        
        self.myTrack = data["race"]["track"]["pieces"]
        self.trackPieceCount = len(self.myTrack)
        self.laneInfo = data["race"]["track"]["lanes"]
        lanes = len(data["race"]["track"]["lanes"])
        
        #print("Game Init: {0}".format(data))

        #Don't re initialize the model after the qualifying roound
        if (self.DataInitialized == False):
            self.model = Model(self.myTrack, self.laneInfo, self.myCarId)

        for p in self.myTrack:
            print p
            #Find the opportunity Cost of using a lane on this track
            #Cost will likely need to be replaced with actual math
            #but for now it goes by a best to worst utility based
            #on the size of the curve and which lane is on the inside
            cost = []
            angle = p.get("angle", 0)
            if (angle < 0):
                for i in range(lanes):
                    cost.append( (i + 1) * p["radius"])
                #print ("Left Turn {0}".format(cost))
            elif (angle > 0):
                for i in range(lanes):
                    cost.append( (lanes - i + 1) * p["radius"])
                #print ("Right Turn {0}".format(cost))
            else:
                for i in range(lanes):
                    cost.append( p["length"])
                #print ("Straight Track {0}".format(cost))
            p["cost"] = cost

        self.ping()

    def getTrackAt(self, ID):
        while(ID >= self.trackPieceCount):
            ID -= self.trackPieceCount
        return self.myTrack[ ID ]

    def shortestPath(self, aStartID, aLane, aEndID):
        #defensive Code
        if (aStartID == aEndID):
            return

        currentState = {}
        currentState["pieceIndex"] = aStartID
        currentState["lane"] = aLane
        currentState["cost"] = 0
        currentState["switchList"] = []

        bestSolution = {}
        bestSolution["cost"] = 9999999

        frontier = [ currentState ]

        #print("Rival Cars {0}".format(self.rivalCars))

        while (len(frontier) > 0):
            #Find the index shortest incomplete path
            fVals = [ node["cost"] for node in frontier]
            minIndex = fVals.index(min(fVals))
            node = frontier.pop(minIndex)

            #Break the loop if the nodes being explored are longer than our best path
            if (node["cost"] > bestSolution["cost"]):
                frontier = []
                break
            #Check for Solution
            if (node["pieceIndex"] == aEndID) and (node["cost"] < bestSolution["cost"]):
                bestSolution = node
                continue

            #Travel down the track until we reach a switch or the end of race, or destination
            track = self.getTrackAt(node["pieceIndex"])
            node["cost"] += track["cost"][ node["lane"] ] #Cost of using this peice on this lane

            #Find if a rival car is in our lane
            if (self.factorRivals == True):
                for color in self.rivalCars:
                    rival = self.rivalCars[color]
                    #print("Rival {0}".format(rival))
                    if (rival["futurePieceIndex"] == node["pieceIndex"]) and (rival["futureLane"] == node["lane"]):
                        node["cost"] += 300
                    
            node["pieceIndex"] += 1

            #Check for Switch
            if (track.get("switch", False) == True):
                #Test Left Switch
                if (node["lane"] > 0):
                    leftSwitch = {}
                    leftSwitch["pieceIndex"] = node["pieceIndex"]
                    leftSwitch["lane"] = node["lane"] - 1
                    leftSwitch["cost"] = node["cost"] + (math.fabs(self.laneInfo[ leftSwitch["lane"] ]["distanceFromCenter"] - self.laneInfo[ node["lane"] ]["distanceFromCenter"]))
                    leftSwitch["switchList"] = []
                    leftSwitch["switchList"] = [item[:] for item in node["switchList"]]
                    leftSwitch["switchList"].append("Left")
                    frontier.append( leftSwitch )
                    
                #Test Right Switch
                if (node["lane"] < (len(self.laneInfo)-1)):
                    rightSwitch = {}
                    rightSwitch["pieceIndex"] = node["pieceIndex"]
                    rightSwitch["lane"] = node["lane"] + 1
                    rightSwitch["cost"] = node["cost"] + (math.fabs(self.laneInfo[ rightSwitch["lane"] ]["distanceFromCenter"] - self.laneInfo[ node["lane"] ]["distanceFromCenter"]))
                    rightSwitch["switchList"] = []
                    rightSwitch["switchList"] = [item[:] for item in node["switchList"]]
                    rightSwitch["switchList"].append("Right")
                    frontier.append( rightSwitch )

                node["switchList"].append("None") #Do nothing at the switch

                    
            frontier.append(node)

        #print(" Best Switches {0}".format(bestSolution["switchList"]))
        self.myActionList = bestSolution["switchList"]
        #return data related to the bet move
        return bestSolution

    def on_turboReady(self, data):
        self.turboReady = True
        self.turboData["TicksLeft"] = data["turboDurationTicks"]
        self.turboData["Ready"] = True
        self.turboData["Active"] = False
        self.turboData["Power"] = data["turboFactor"]

    def msg_loop(self):
        if (VERBOSE): print "in msg_loop()"
        msg_map = {
            'join': self.on_join,
            'yourCar' : self.on_your_car,
            'gameStart': self.on_game_start,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_gameInit,
            'turboAvailable' :self.on_turboReady,
            'spawn' : self.on_spawn,
		}
		
        tic_msg_map = {
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
        }

        if (VERBOSE): print "in msg_loop(), entering socket read loop"

        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            if(VERBOSE): 
                #print "--got message--", msg_type
                if ('gameTick' in msg.keys() and msg['gameTick']%10 == 0): print msg['gameTick']

            if msg_type in msg_map:
                msg_map[msg_type](data)
            elif msg_type in tic_msg_map:
                #gameTick not sent during initiation
                if ('gameTick' in msg.keys()): self.gameTick = float(msg['gameTick'])  
                tic_msg_map[msg_type](data, self.gameTick)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

        if (VERBOSE): print "in msg_loop(), broke socket read loop"

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))  
        if(VERBOSE): print("Creating bot")
        bot = NoobBot(s, name, key)
        #bot = InfoBot(s, name, key)
        if(VERBOSE): print("Running bot")
        bot.run()
