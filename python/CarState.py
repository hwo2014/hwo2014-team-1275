import math

from hwoutil import *


class CarState(object):
    '''
    The state of a car.

    NOTE: smaller mem footprint and *slightly* faster with __slots__
    http://stackoverflow.com/questions/14118564/how-does-slots-avoid-a-dictionary-lookup
    '''
    __slots__ = [
        'track',
        'laneInfo',
        'model',
        'gameTick',
        'pieceIndex',
        'inPieceDistance',
        'startLaneIndex',
        'endLaneIndex',
        'speed',
        'acceleration',
        'angle',
        'turboAvailable',
        'turboDurationTicks',
        'turboActive',
        'turboStartTick',
        'maxSpeed',
        'maxAccel',
        'maxDecel',
        'currThrottle',
        'maxSlip',
        'deltaSlip',
        'isCrashed',
        'lookAhead',
        'v0', 'v1', 'v2', 'k', 'internalTick'
        ]

    def __init__(self, track, model, laneInfo, carDict=None, gameTick=None):
        self.track = track
        self.laneInfo = laneInfo
        self.model = model

        self.speed = 0.0
        self.acceleration = 0.0
        self.turboAvailable = False
        self.turboDurationTicks = -1
        self.turboActive = False
        self.turboStartTick = -1

        self.currThrottle = 0.0
        self.maxSlip = 100
        self.deltaSlip = 0
        self.lookAhead = 2

        self.isCrashed = False



        if carDict != None:
            self.gameTick = gameTick
            self.pieceIndex = carDict['piecePosition']['pieceIndex']
            self.inPieceDistance = carDict['piecePosition']['inPieceDistance']
            self.startLaneIndex = carDict['piecePosition']['lane']['startLaneIndex']
            self.endLaneIndex = carDict['piecePosition']['lane']['endLaneIndex']
            self.angle = carDict['angle']

        for piece in self.track:
            piece["speedLimit"] = [99, 99, 99, 99]  #Max of Four Lanes

    def reset(self):
        self.speed = 0.0
        self.acceleration = 0.0
        self.turboAvailable = False
        self.turboDurationTicks = -1
        self.turboActive = False
        self.turboStartTick = -1

        self.currThrottle = 0.0
        self.deltaSlip = 0

        self.isCrashed = False

    def crashed(self):
        self.currThrottle = 0.0
        self.isCrashed = True
        print("Crashed at Angle {0}".format(self.angle))


    def spawned(self):
        self.speed = 0.0
        self.acceleration = 0.0
        self.currThrottle = 0.1
        self.isCrashed = False
        maxSpeed = None
        self.lookAhead += 1
        return

        for p in range(0,len(self.track)):
            piece = self.track[p]
            if (piece.get("angle",0) == 0):
                maxSpeed = self.model.getTerminalVelocity(1.0)
                if (maxSpeed == None):
                    continue
                for lane in range(0, len(self.laneInfo)):
                    piece["speedLimit"][lane] = int(maxSpeed * 100) / 100.0
            else:
                for lane in range(0, len(self.laneInfo)):
                    maxSpeed = self.model.getEstMaxTurnSpeed(piece, lane, lane)

                    if (maxSpeed == None):
                        continue

                    maxSpeed = int(maxSpeed * 100) / 100.0
                    if (maxSpeed < piece["speedLimit"][lane]):
                        piece["speedLimit"][lane] = maxSpeed
            #print("Piece {0}".format(piece))

            '''
                currPieceIndex = p
                for x in range(1, 3):
                    lastSpeedLimit = self.track[currPieceIndex]["speedLimit"][lane]
                    currPieceIndex -= x
                    if (currPieceIndex < 0):
                        currPieceIndex += len(self.track)
                    if (self.track[currPieceIndex]["speedLimit"][lane] >= lastSpeedLimit):
                        self.track[currPieceIndex]["speedLimit"][lane] = (lastSpeedLimit + self.track[currPieceIndex]["speedLimit"][lane]) * 0.5
            '''        
        #for piece in self.track:
        #    print("Track {0} {1}".format(piece["speedLimit"], piece))

    def updateState(self, carDict, gameTick):
        #no time has passed
        if (gameTick - self.gameTick) == 0:
            return 

        if (self.isCrashed):
            return


        #speed
        if (carDict['piecePosition']['pieceIndex'] == self.pieceIndex):
            currSpeed = float(carDict['piecePosition']['inPieceDistance'] - self.inPieceDistance)/(gameTick - self.gameTick)
        else:
            dist = getPieceLength(self.track[self.pieceIndex], self.laneInfo, self.startLaneIndex, self.endLaneIndex) - self.inPieceDistance + carDict['piecePosition']['inPieceDistance']
            currSpeed = dist/(gameTick - self.gameTick)

        #acceleration
        self.acceleration = (currSpeed - self.speed)/(gameTick - self.gameTick)

        #how Much the car is slipping
        self.deltaSlip = carDict['angle'] - self.angle

        projectedSlip = self.deltaSlip
        currPieceIndex = self.pieceIndex
        currDistanceInPiece =  self.inPieceDistance
        currTrackAngle = self.track[self.pieceIndex].get("angle", 0.0)
        '''
        for i in range(0, 5):
            #Advance down the track
            currLength = getPieceLength(self.track[currPieceIndex], self.laneInfo, self.startLaneIndex, self.endLaneIndex)
            currDistanceInPiece += currSpeed  #Needs to take into account acceleration
            if (currDistanceInPiece > currLength):
                currPieceIndex += 1
                if (currPieceIndex >= len(self.track)):
                    currPieceIndex = 0
                currDistanceInPiece -= currLength

            #Add another delta Angle
            projectedSlip += self.deltaSlip

            #check if we are still on an angle track, and that the angle hasn't changed directions (pos -> neg)
            angle = self.track[currPieceIndex].get("angle", 0)
            if (angle == 0) or ( (currTrackAngle < 0) != (angle < 0) ):
                break
        '''

        #Get some information about slips
        if (self.track[self.pieceIndex].get("angle",0) != 0) and ( abs(self.deltaSlip) > 0.0) and (abs(self.angle) < 0.01):
            R = getTurnRadius(self.track[self.pieceIndex], self.laneInfo, self.startLaneIndex, self.endLaneIndex)
            B = math.degrees( currSpeed / R )
            Bslip = B - carDict['angle']
            Vthresh = math.radians(Bslip) * R
            self.maxSlip = (Vthresh * Vthresh) / R
            VMaxNoSlip = math.sqrt(self.maxSlip * R)  #Calculate Max Slip Speed

            #print("Cforce: {0}  Delta: {1}  Vthresh {2}".format(self.model.maxCForce, self.deltaSlip, Vthresh))

            if (self.model.maxCForce == None) or (self.model.maxCForce < self.maxSlip ):
                self.model.maxCForce = self.maxSlip * (1.8 - (self.lookAhead * 0.2))


            if (VMaxNoSlip < self.model.getTerminalVelocity(1.0)):
                #Apply max slip speed to all 
                #for piece in self.track:
                maxSpeed = None

                for p in range(0,len(self.track)):
                    piece = self.track[p]
                    if (piece.get("angle",0) == 0):
                        maxSpeed = self.model.getTerminalVelocity(1.0)
                        if (maxSpeed == None):
                            continue
                        for lane in range(0, len(self.laneInfo)):
                            piece["speedLimit"][lane] = int(maxSpeed * 100) / 100.0
                    else:
                        for lane in range(0, len(self.laneInfo)):
                            maxSpeed = self.model.getEstMaxTurnSpeed(piece, lane, lane)

                            if (maxSpeed == None):
                                continue

                            maxSpeed = int(maxSpeed * 100) / 100.0
                            if (maxSpeed < piece["speedLimit"][lane]):
                                piece["speedLimit"][lane] = maxSpeed


            angle = abs(self.track[self.pieceIndex].get("angle", 0.0))
            #print("Track Angle: {0}   Length {1}  VMaxNoSlip: {2}".format(angle, R, VMaxNoSlip))
            #print("Throttle: {1:.2} Speed: {0:.2} Angle: {2:.2}".format(currSpeed, self.currThrottle, self.deltaSlip))
        

        self.gameTick = gameTick
        self.speed = currSpeed
        self.pieceIndex = carDict['piecePosition']['pieceIndex']
        self.inPieceDistance = carDict['piecePosition']['inPieceDistance']
        self.startLaneIndex = carDict['piecePosition']['lane']['startLaneIndex']
        self.endLaneIndex = carDict['piecePosition']['lane']['endLaneIndex']
        self.angle = carDict['angle']


        #Breaking when approaching Curves
        topSpeed = 99
        for i in range(0, self.lookAhead):
            nextPieceIndex = self.pieceIndex + i
            if (nextPieceIndex >= len(self.track)):
                nextPieceIndex -= len(self.track)

            speedLim = self.track[nextPieceIndex]["speedLimit"][self.endLaneIndex]
            if (speedLim < topSpeed):
                topSpeed = speedLim
        if (currSpeed >= topSpeed ):
            #self.currThrottle =  self.model.getThrottleFromVelocity(speedLim * 0.25) # 0.2
            #if (self.currThrottle < 0.2):
            self.currThrottle = 0.01
        else:
            self.currThrottle = self.model.getThrottleFromVelocity(topSpeed * 0.95)

        #----------
        ### if the next peice is a turn, get the max speed for that turn, and decelarate accordingly
        '''if ('angle' in self.track[nextPieceIndex]):
            maxSpeed = self.model.getEstMaxTurnSpeed(self.track[nextPieceIndex], self.endLaneIndex, self.endLaneIndex)

            if (maxSpeed): 
                optThrottle = self.getOptimalDecellerationThrottle(maxSpeed, self, nextPieceDist)
                print "EstMaxVelocity for pieceId", nextPieceIndex, ":", maxSpeed, "OptThrottle:", optThrottle
                self.currThrottle = optThrottle if optThrottle else .8

            else:
                print "couldn't find opt throttle.  Full Speed!"
                self.currThrottle = 1.0
        else:
            self.currThrottle = 1.0'''
        

        
        if (self.currThrottle > 1.0):
            self.currThrottle = 1.0
        if (self.currThrottle < 0.0):
            self.currThrottle = 0.0
        


        #print("Goal Speed: {0}  k: {1}   Throttle: {2}".format(self.track[nextPieceIndex]["speedLimit"][self.endLaneIndex], self.k, self.currThrottle))

    def getOptimalDecellerationThrottle(self, targetSpeed, startState, totalDist):
        #dumb dumb dumb
        for throttle in range(10,1,-1):
            throttle = throttle / 10.0
            ticks = self.model.getTicksToGetToSpeed(throttle, targetSpeed, startState)
            dist = self.model.getDistanceTravledAfterTicks(throttle, ticks, startState)
            print "==throttle:", throttle, "ticks: ", ticks, "dist:", dist, 
            print "startSpeed", self.speed ,"targetspeed", targetSpeed, "totalDist:",totalDist
            if (dist<totalDist):
                return throttle
        return None


class CarStates(object):
    '''
    states of all the cars
    '''
    def __init__(self, track, model, laneInfo, carsDict, gameTick):
        self.cars = {}
        for carDict in carsDict:
            self.cars[carDict['id']['color']] = CarState(track, laneInfo, model, carDict, gameTick)

    def updateState(self, carsDict, gameTick):
        for carDict in carsDict:
            self.cars[carDict['id']['color']].updateState(carDict, gameTick)

    def reset(self):
        for car in self.cars.values():
            car.reset()
