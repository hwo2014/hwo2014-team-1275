import math
from collections import deque
import copy

from CarState import *
from hwoutil import *
from hwoutil import getTurnRadius

'''
Model the physics of the game.  Based on some combination of physics modeling and ML techniques.
Model is trained as the race proceeds.
'''
class Model(object):

    def __init__(self, track, laneInfo, myCarId):
        self.track = track
        self.laneInfo = laneInfo
        self.myCarId = myCarId
        self.stateHistory = deque([], 4) #only store the last 4 states

        self.trainedLinearParameters = False
        self.trainedAngularParameters = True

        #linear parameters
        self.k = -1
        self.m = -1

        #angular parameters
        # pretty simple, probably sort of wrong.  the max "Centrifigual Force" before a crash occurs.
        # using this, calculate the max velocity that can be used to take a turn.
        self.maxCForce = None 

        self.VERBOSE = True

    #---- Training -----
    def trainModel(self, currentCarStates):
        '''
        train the model parameters based on the data.  Call this when a 'carPositions' or 'crash' message is sent.
        '''

        if not(self.trainedLinearParameters and self.trainedAngularParameters):
            self.stateHistory.append(copy.deepcopy(currentCarStates))

            if not(self.trainedLinearParameters):
                self.trainLinearParameters()

            if not(self.trainedAngularParameters):
                self.trainAngularParameters()
        else:
            pass

    def trainFromCrash(self, car):
        if (getTurnRadius(self.track[car.pieceIndex], self.laneInfo, car.startLaneIndex, car.endLaneIndex) > 0):
            if (self.VERBOSE): print "---LEARNING CRASH PARAMETERS: v = ", car.speed,"----"
            calcMaxCForce = pow(car.speed, 2)/ (getTurnRadius(self.track[car.pieceIndex], self.laneInfo, car.startLaneIndex, car.endLaneIndex))

            if ((self.maxCForce == None) or (calcMaxCForce < self.maxCForce)):
                if(self.VERBOSE): print "Updating maxCForce from ", self.maxCForce, "to", calcMaxCForce
                self.maxCForce = calcMaxCForce
            else:
                if(self.VERBOSE): print "Calculated maxCForce ", calcMaxCForce, "is greater then current best max,", self.maxCForce,  ". Ignoring."

    def trainLinearParameters(self):
        if (((len(self.stateHistory)) >= 4) and
            (self.stateHistory[0].cars[self.myCarId['color']].throttle != 0) and
            (self.stateHistory[0].cars[self.myCarId['color']].throttle ==  self.stateHistory[1].cars[self.myCarId['color']].throttle) and
            (self.stateHistory[0].cars[self.myCarId['color']].throttle ==  self.stateHistory[2].cars[self.myCarId['color']].throttle) and
            (self.stateHistory[0].cars[self.myCarId['color']].throttle ==  self.stateHistory[3].cars[self.myCarId['color']].throttle) and
            (self.stateHistory[0].cars[self.myCarId['color']].speed == 0) and
            (self.stateHistory[1].cars[self.myCarId['color']].speed != 0)
            ):

            if (self.VERBOSE): print "---TRAINING LINEAR PARAMETERS----"

            v1 = self.stateHistory[1].cars[self.myCarId['color']].speed
            v2 = self.stateHistory[2].cars[self.myCarId['color']].speed
            v3 = self.stateHistory[3].cars[self.myCarId['color']].speed
            th = self.stateHistory[1].cars[self.myCarId['color']].throttle

            if (self.VERBOSE): 
                print "v1", v1, "v2", v2, "v3", v3, "th", th
                #print self.stateHistory[1].cars[self.myCarId['color']].gameTick
                #print self.stateHistory[2].cars[self.myCarId['color']].gameTick
                #print self.stateHistory[3].cars[self.myCarId['color']].gameTick

            self.k =  float((v1 - ( v2 - v1 )) / (pow(v1, 2))*th)
            #self.m = float(1.0 / ( math.log(( v3 - ( th / self.k )) / (v2 - ( th / self.k ) ) ) / ( -1.0 * self.k )))
            self.m = v1 / th

            print "drag: ", self.k, " enginePower: ", self.m
            self.trainedLinearParameters = True

    def trainAngularParameters(self):
        pass



    #----- Prediction ----
    def getTerminalVelocity(self, throttle):
        if (self.trainedLinearParameters):
            return throttle/self.k
        else:
            return None

    def getThrottleFromVelocity(self, velocity):
        if (self.trainLinearParameters):
            if (velocity > (1.0 / self.k)):
                velocity = 1.0 / self.k
            return velocity * self.k
        else:
            return 1.0

    def getSpeedAfterTicks(self, throttle, ticks, carState):
        if (self.trainedLinearParameters):
            return (carState.speed - (throttle/self.k) ) * math.exp( ( - self.k * ticks ) / self.m ) + ( throttle/self.k )

    def getDistanceTravledAfterTicks(self, throttle, ticks, carState):
        if (ticks == None): return None
        if (self.trainedLinearParameters):
            print carState.speed
            return ( self.m/self.k ) * ( carState.speed - ( throttle/self.k ) ) * ( 1.0 - math.exp( ( -self.k*ticks ) / self.m ) ) + ( throttle/self.k ) * ticks + carState.inPieceDistance
            '''
            if (carState):
                return ( self.m/self.k ) * ( carState.speed - ( throttle/self.k ) ) * ( 1.0 - math.exp( ( -self.k*ticks ) / self.m ) ) + ( throttle/self.k ) * ticks + carState.inPieceDistance
            else:
                return ( self.m/self.k ) * ( 0.0 - ( throttle/self.k ) ) * ( 1.0 - math.exp( ( -self.k*ticks ) / self.m ) ) + ( throttle/self.k ) * ticks
            '''

    def getTicksToGetToSpeed(self, throttle, targetSpeed, carState):
        if ((self.trainedLinearParameters) and
            (targetSpeed - (throttle/self.k) )/(carState.speed - ( throttle/self.k)) > 0):
            return (math.log((targetSpeed - (throttle/self.k) )/(carState.speed - ( throttle/self.k))) * self.m) / (-1.0 * self.k)
        else:
            return None

    #--
    def getEstMaxTurnSpeed(self, pieceMap, startLaneIndex, endLaneIndex):
        r = getTurnRadius(pieceMap, self.laneInfo, startLaneIndex, endLaneIndex)
        if ((self.maxCForce != None) and r > 0):
            return math.sqrt(self.maxCForce * r )
        else:
            return None

'''
    def getNextState(self, startState, throttle, switch, turboStart):
        
        #Approximate the next state based on the current model, the startState, and current car action
        #Returns the end state and a the probability of confidence in the result based on the reliability of the model
        
        return [carState, confidence]

    def getOptimalPieceCost(self, startState, pieceIndex, switch, turbo):
        
        #Approximate the optimal time to traverse the piece given the input state and whether a switch action and/or turbo action should be used at the beginning of this piece.
        #Relies on the current model.
        #Returns the optimal traversal time, the end state, the actions needed to achieve the optimal traverse time, and a confidence probability based on the reliability of the model
    
        return [traverseTime, endState, actionPlan[], confidence]
'''