import math

from hwoutil import *


class CarState(object):
    '''
    The state of a car.

    NOTE: smaller mem footprint and *slightly* faster with __slots__
    http://stackoverflow.com/questions/14118564/how-does-slots-avoid-a-dictionary-lookup
    '''
    __slots__ = [
        'track',
        'laneInfo',
        'gameTick',
        'pieceIndex',
        'inPieceDistance',
        'startLaneIndex',
        'endLaneIndex',
        'speed',
        'acceleration',
        'angle',
        'throttle',
        'turboAvailable',
        'turboDurationTicks',
        'turboActive',
        'turboStartTick'
        ]

    def __init__(self, track, laneInfo, carDict=None, gameTick=None):
        self.track = track
        self.laneInfo = laneInfo
        self.speed = 0.0
        self.acceleration = 0.0
        self.turboAvailable = False
        self.turboDurationTicks = -1
        self.turboActive = False
        self.turboStartTick = -1
        self.throttle = -1
        if carDict != None:
            self.gameTick = gameTick
            self.pieceIndex = carDict['piecePosition']['pieceIndex']
            self.inPieceDistance = carDict['piecePosition']['inPieceDistance']
            self.startLaneIndex = carDict['piecePosition']['lane']['startLaneIndex']
            self.endLaneIndex = carDict['piecePosition']['lane']['endLaneIndex']
            self.angle = carDict['angle']
        

    def updateState(self, carDict, gameTick, throttle = -1):
        #no time has passed
        if (gameTick - self.gameTick) == 0:
            return 

        #speed
        if (carDict['piecePosition']['pieceIndex'] == self.pieceIndex):
            currSpeed = float(carDict['piecePosition']['inPieceDistance'] - self.inPieceDistance)/(gameTick - self.gameTick)
        else:
            dist = getPieceLength(self.track[self.pieceIndex], self.laneInfo, self.startLaneIndex, self.endLaneIndex) - self.inPieceDistance + carDict['piecePosition']['inPieceDistance']
            currSpeed = dist/(gameTick - self.gameTick)

        #acceleration
        self.acceleration = (currSpeed - self.speed)/(gameTick - self.gameTick)

        self.gameTick = gameTick
        self.speed = currSpeed
        self.pieceIndex = carDict['piecePosition']['pieceIndex']
        self.inPieceDistance = carDict['piecePosition']['inPieceDistance']
        self.startLaneIndex = carDict['piecePosition']['lane']['startLaneIndex']
        self.endLaneIndex = carDict['piecePosition']['lane']['endLaneIndex']
        self.angle = carDict['angle']
        self.throttle = throttle


class CarStates(object):
    '''
    states of all the cars
    '''
    def __init__(self, track, laneInfo, carsDict, gameTick):
        self.cars = {}
        for carDict in carsDict:
            self.cars[carDict['id']['color']] = CarState(track, laneInfo, carDict, gameTick)

    def updateState(self, carsDict, gameTick, myCarId, myThrottle):
        for carDict in carsDict:
            if carDict['id'] == myCarId:
                self.cars[carDict['id']['color']].updateState(carDict, gameTick, myThrottle)
            else:
                self.cars[carDict['id']['color']].updateState(carDict, gameTick)