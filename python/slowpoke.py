import json
import socket
import sys
import math
import random


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.mytrack = {}
        self.lapsRemaining = 99
        #Ideal line for Keimola
        self.myActionList = [ "Right", "None", "Left"]
        self.myThrottle = [0.5, 0.4, 0.6]
        self.lastSwitchUsed = -1
        self.lastLocID = -1
        self.trackPieceCount = 0
        self.laneInfo = []
        

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("joinRace", {"trackName": "keimola", "carCount":4,"botId":{"name": self.name,"key": self.key}})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        myCar = []
        for car in data:
            if (car["id"]["name"] == self.name):
                myCar = car

        myLocID = myCar["piecePosition"]["pieceIndex"]
        nextID = myLocID + 1
        if (nextID >= self.trackPieceCount): #Perform a switch command before the switch happens
            nextID = 0
        if (myLocID != self.lastLocID):
            self.lastLocID = myLocID
            #print("Car just entered: {0}".format(self.myTrack[myLocID]))

        #determine and send switch
        #!!! Determine when we need to recalculate the path (Before switch?  After? Both?)
        if (self.myActionList) and (self.myTrack[ nextID ].get("switch", False) == True) and (myLocID != self.lastSwitchUsed):
            #(self, aStartID, aLane, aEndID
            #print("StartLane Index {0}".format(myCar["piecePosition"]["lane"]["startLaneIndex"]))
            #self.shortestPath(myLocID, myCar["piecePosition"]["lane"]["startLaneIndex"], myLocID+25)
            self.lastSwitchUsed = myLocID
            action = random.choice(self.myActionList)
            #action = self.myActionList.pop(0)
            print("Switching to: {0}".format(action))
            if (action != "None"):
                self.msg("switchLane", action)
                return

                
        self.throttle(random.choice(self.myThrottle))

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    #Create Data structure to hold the track data recieved
    def on_gameInit(self, data):
        self.lapsRemaining = data["race"]["raceSession"]["laps"]
        print ("Laps Remaining: {0}". format(self.lapsRemaining))
        
        self.myTrack = data["race"]["track"]["pieces"]
        self.trackPieceCount = len(self.myTrack)
        self.laneInfo = data["race"]["track"]["lanes"]
        lanes = len(data["race"]["track"]["lanes"])
        
	#print("Game Init: {0}".format(data))

	for p in self.myTrack:
            print p
            #Find the opportunity Cost of using a lane on this track
            #Cost will likely need to be replaced with actual math
            #but for now it goes by a best to worst utility based
            #on the size of the curve and which lane is on the inside
            cost = []
            angle = p.get("angle", 0)
            if (angle < 0):
                for i in range(lanes):
                    cost.append( (i + 1) * p["radius"])
                #print ("Left Turn {0}".format(cost))
            elif (angle > 0):
                for i in range(lanes):
                    cost.append( (lanes - i + 1) * p["radius"])
                #print ("Right Turn {0}".format(cost))
            else:
                for i in range(lanes):
                    cost.append( p["length"])
                #print ("Straight Track {0}".format(cost))
            p["cost"] = cost


            
	self.ping()

    def getTrackAt(self, ID):
        while(ID >= self.trackPieceCount):
            ID -= self.trackPieceCount
        return self.myTrack[ ID ]

    def shortestPath(self, aStartID, aLane, aEndID):
        #defensive Code
        if (aStartID == aEndID):
            return

        currentState = {}
        currentState["pieceIndex"] = aStartID
        currentState["lane"] = aLane
        currentState["cost"] = 0
        currentState["switchList"] = []

        bestSolution = {}
        bestSolution["cost"] = 9999999

        frontier = [ currentState ]

        while (len(frontier) > 0):
            #Find the index shortest incomplete path
            fVals = [ node["cost"] for node in frontier]
            minIndex = fVals.index(min(fVals))
            node = frontier.pop(minIndex)

            #Break the loop if the nodes being explored are longer than our best path
            if (node["cost"] > bestSolution["cost"]):
                frontier = []
                break
            #Check for Solution
            if (node["pieceIndex"] == aEndID) and (node["cost"] < bestSolution["cost"]):
                bestSolution = node
                continue

            #Travel down the track until we reach a switch or the end of race, or destination
            track = self.getTrackAt(node["pieceIndex"])
            node["cost"] += track["cost"][ node["lane"] ] #Cost of using this peice on this lane
            node["pieceIndex"] += 1

            #Check for Switch
            if (track.get("switch", False) == True):
                #Test Left Switch
                if (node["lane"] > 0):
                    leftSwitch = {}
                    leftSwitch["pieceIndex"] = node["pieceIndex"]
                    leftSwitch["lane"] = node["lane"] - 1
                    leftSwitch["cost"] = node["cost"] + (math.fabs(self.laneInfo[ leftSwitch["lane"] ]["distanceFromCenter"] - self.laneInfo[ node["lane"] ]["distanceFromCenter"]))
                    leftSwitch["switchList"] = []
                    leftSwitch["switchList"] = [item[:] for item in node["switchList"]]
                    leftSwitch["switchList"].append("Left")
                    frontier.append( leftSwitch )
                    
                #Test Right Switch
                if (node["lane"] < (len(self.laneInfo)-1)):
                    rightSwitch = {}
                    rightSwitch["pieceIndex"] = node["pieceIndex"]
                    rightSwitch["lane"] = node["lane"] + 1
                    rightSwitch["cost"] = node["cost"] + (math.fabs(self.laneInfo[ rightSwitch["lane"] ]["distanceFromCenter"] - self.laneInfo[ node["lane"] ]["distanceFromCenter"]))
                    rightSwitch["switchList"] = []
                    rightSwitch["switchList"] = [item[:] for item in node["switchList"]]
                    rightSwitch["switchList"].append("Right")
                    frontier.append( rightSwitch )

                node["switchList"].append("None") #Do nothing at the switch

                    
            frontier.append(node)

        #print(" Best Switches {0}".format(bestSolution["switchList"]))
        self.myActionList = bestSolution["switchList"]
        

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
	    'gameInit': self.on_gameInit,
		}
		
	socket_file = s.makefile()
	line = socket_file.readline()
	while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        
        bot = NoobBot(s, name, key)
        bot.run()

