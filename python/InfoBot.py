import json
import socket
import sys
import datetime
import math

from hwoutil import getTurnRadius
from Model import *
import CleanCarState

class InfoBot(object):

    def __init__(self, socket, name, key):
        print "===InfoBot==="
        self.socket = socket
        self.name = name
        self.key = key

        # global static game parameters
        self.mytrack = {}
        self.laneInfo = {}
        self.myCarId = []

        # global dynamic game parameters
        self.gameTick = 0.0
        self.carStates = False
        self.model = None

        # race history
        self.gameTickHist = [0]
        self.throttleHist = [0]
        self.speedHist = [0]
        self.accelerationHist = [0]
        self.angleHist = [0]
        self.pieceAngleHist = [0]
        self.pieceRadHist = [0]


    #-----Send Message Methods----
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    #------Handle Message Methods----
    def on_lap_finished(self, data):
        self.lapsRemaining = self.lapsRemaining - 1
        print ("Laps Remaining: {0}". format(self.lapsRemaining))

    #Create Data structure to hold the track data recieved
    def on_gameInit(self, data):
        self.lapsRemaining = data["race"]["raceSession"]["laps"]
        print ("Laps Remaining: {0}". format(self.lapsRemaining))
        
        self.myTrack = data["race"]["track"]["pieces"]
        self.trackPieceCount = len(self.myTrack)
        self.laneInfo = data["race"]["track"]["lanes"]
        lanes = len(data["race"]["track"]["lanes"])

        self.model = Model(self.myTrack, self.laneInfo, self.myCarId)
        
        #print("Game Init: {0}".format(data))

        for p in self.myTrack:
            print p
            #Find the opportunity Cost of using a lane on this track
            #Cost will likely need to be replaced with actual math
            #but for now it goes by a best to worst utility based
            #on the size of the curve and which lane is on the inside
            cost = []
            angle = p.get("angle", 0)
            if (angle < 0):
                for i in range(lanes):
                    cost.append( (i + 1) * p["radius"])
                #print ("Left Turn {0}".format(cost))
            elif (angle > 0):
                for i in range(lanes):
                    cost.append( (lanes - i + 1) * p["radius"])
                #print ("Right Turn {0}".format(cost))
            else:
                for i in range(lanes):
                    cost.append( p["length"])
                #print ("Straight Track {0}".format(cost))
            p["cost"] = cost

        self.ping()

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_your_car(self, data):
        print("Your car")
        self.myCarId = data

    def on_game_start(self, data):
        print("Race started")
        self.gameTick = 0
        self.ping()

    def on_car_positions(self, data, gameTick):
        #print("Car position, gameTick=",gameTick)

        constThrottle = 1.0

        ##########THIS MUST BE DONE############
        #constant throttle
        self.throttle(constThrottle)

        #--calculate current state--
        #first game tic, get all car states
        if (self.carStates == False):
            self.carStates = CleanCarState.CarStates(self.myTrack, self.laneInfo, data, gameTick)

        else:
            self.carStates.updateState(data, gameTick, self.myCarId, constThrottle)

        #--update the model--
        self.model.trainModel(self.carStates)
        ##################

        #print "est term vel:", self.model.getTerminalVelocity(0.0), self.model.getTerminalVelocity(.1), self.model.getTerminalVelocity(.5)


        #--gather data--
        self.gameTickHist.append(gameTick)
        self.throttleHist.append(constThrottle)
        self.speedHist.append(self.carStates.cars[self.myCarId['color']].speed)
        self.accelerationHist.append(self.carStates.cars[self.myCarId['color']].acceleration)
        self.angleHist.append(self.carStates.cars[self.myCarId['color']].angle)

        # in a turn piece
        myCar = {}
        for car in data:
            if car['id'] == self.myCarId: myCar = car 

        if ('length' not in self.myTrack[myCar['piecePosition']['pieceIndex']]):
            self.pieceAngleHist.append(self.myTrack[myCar['piecePosition']['pieceIndex']]['angle'])
            self.pieceRadHist.append(getTurnRadius(self.myTrack[myCar['piecePosition']['pieceIndex']],
                self.laneInfo,
                myCar['piecePosition']['lane']['startLaneIndex'], 
                myCar['piecePosition']['lane']['endLaneIndex']))
        else:
            self.pieceAngleHist.append(0)
            self.pieceRadHist.append(0)


    def on_crash(self, data, gameTick):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended, printing data")
        print "Est Terminal Velocity ", self.model.getTerminalVelocity(.6)

        #filename = "data/race" + datetime.datetime.today().strftime('%y%m%d_%H%m%S') +  self.myCarId['name'] + "_" + self.myCarId['color'] + ".txt"
        filename = "data/racetest_" +  self.myCarId['name'] + "_" + self.myCarId['color'] + ".csv"
        outfile = open(filename, 'w')
        outfile.write('gameTick,throttle,angle,speed,acceleration,PieceAngle,PieceRadius\n')
        for i in range(0, len(self.gameTickHist)):
            outfile.write(str(self.gameTickHist[i]))
            outfile.write("," + str(self.throttleHist[i]))
            outfile.write("," + str(self.angleHist[i]))
            outfile.write("," + str(self.speedHist[i]))
            outfile.write("," + str(self.accelerationHist[i]))
            outfile.write("," + str(self.pieceAngleHist[i]))
            outfile.write("," + str(self.pieceRadHist[i]) + "\n")

        '''
        outfile.write("\t".join([str(d) for d in self.gameTickHist]) + "\n")
        outfile.write("\t".join([str(d) for d in self.throttleHist]) + "\n")
        outfile.write("\t".join([str(d) for d in self.angleHist]) + "\n")
        outfile.write("\t".join([str(d) for d in self.speedHist]) + "\n")
        outfile.write("\t".join([str(d) for d in self.accelerationHist]) + "\n")
        outfile.write("\t".join([str(d) for d in self.pieceAngleHist]) + "\n")
        outfile.write("\t".join([str(d) for d in self.pieceRadHist]) + "\n")
        '''
        outfile.close()

        #---reset game parameters---
        # global game parameters
        self.mytrack = {}
        self.laneInfo = {}
        self.myCarId = []

        # prev tick
        self.myCarState = False
        self.gameTick = 0.0

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar' : self.on_your_car,
            'gameStart': self.on_game_start,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit' : self.on_gameInit,
            'lapFinished' : self.on_lap_finished,
        }
        tic_msg_map = {
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            elif msg_type in tic_msg_map:
                #gameTick not sent during initiation
                if ('gameTick' in msg.keys()): self.gameTick = float(msg['gameTick'])  
                tic_msg_map[msg_type](data, self.gameTick)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
