import math

#-----Piece Utility Methods---
# Calculate the length of a given piece given the lane id
#
# If the piece is a straight, just return length
# If the piece is a turn, calculate the length taking laneId into account
def getPieceLength(pieceMap, laneInfo, startLaneIndex, endLaneIndex):
    if ('length' in pieceMap):
        return float(pieceMap['length'])
    else:
        radius = getTurnRadius(pieceMap, laneInfo, startLaneIndex, endLaneIndex)
        return abs((pieceMap['angle']/360.0) * (2 * math.pi * (radius)))

# Calculate the radius of a round piece, taking the lane offset into account
# TODO - right now only using startLaneIndex
def getTurnRadius(pieceMap, laneInfo, startLaneIndex, endLaneIndex):
	if 'angle' in pieceMap:
	    for l in laneInfo:
	        if l['index'] == startLaneIndex:
	            laneOffset = float(l['distanceFromCenter']) * (pieceMap['angle']/abs(pieceMap['angle']))

	    return pieceMap['radius'] - laneOffset
	else:
		return 0.0